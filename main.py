import argparse
from pathlib import Path
from typing import Optional

from hloc import extract_features, match_features, reconstruction, visualization, pairs_from_exhaustive
from hloc.utils import viz_3d


def main(image_path: Optional[Path] = "default") -> None:
    images = Path("datasets") / image_path
    outputs = Path("outputs") / image_path

    sfm_pairs = outputs / "pairs-sfm.txt"
    loc_pairs = outputs / "pairs-loc.txt"
    sfm_dir = outputs / "sfm"
    features = outputs / "features.h5"
    matches = outputs / "matches.h5"

    feature_conf = extract_features.confs['superpoint_aachen']
    matcher_conf = match_features.confs['superglue']

    references = [p.relative_to(images).as_posix() for p in images.iterdir()]

    extract_features.main(feature_conf, images, image_list=references, feature_path=features)
    print("Feature extraction complete")
    pairs_from_exhaustive.main(sfm_pairs, image_list=references)
    match_features.main(matcher_conf, sfm_pairs, features=features, matches=matches); #superglue
    print("Feature matching complete")

    # model = reconstruction.main(sfm_dir, images, sfm_pairs, features, matches, image_list=references)
    # print("Reconstruction complete")

    # model.export_PLY(f"{outputs / image_path}.ply")
    # fig = viz_3d.init_figure()
    # viz_3d.plot_reconstruction(fig, model, color="rgba(255,255,255,0.8)")
    # fig.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", type=Path)
    args = parser.parse_args()
    main(args.d) if args.d else main()
